// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB7Azn0yUkXKPf--eJc6zC702zdOJEiGr8",
    authDomain: "ng-giza.firebaseapp.com",
    databaseURL: "https://ng-giza.firebaseio.com",
    projectId: "ng-giza",
    storageBucket: "ng-giza.appspot.com",
    messagingSenderId: "171533990019"
  }
};
