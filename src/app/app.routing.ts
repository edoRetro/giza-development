import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent,
        data: {
          title: 'Login'
        }
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },  
  { path: '', component: FullLayoutComponent, 
    data: { title: 'Inicio' },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      }
    ]}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],  
})
export class AppRoutingModule {}
