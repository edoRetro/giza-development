//Input = 2, Output = 02, parses number lowers than 10
export function parseStringToDate(n) {
    return n > 9 ? "" + n: "0" + n;
}

export function getDateObjectFromDate(n: Date){
    return {year: n.getFullYear(), 
            month: n.getMonth() + 1, 
            day: n.getDate()}
}

//Input should be in format dd/mm/yyyy
export function getDateObjectFromString(n: string){
    console.log(JSON.stringify(n));
    let splited = n.split("/");
    return {
        year: removeZero(splited[2]), 
        month: removeZero(splited[1]), 
        day: removeZero(splited[0])
    }
}



function removeZero(n: string){
    return n.substr(0, 1) == '0' ? n.substr(1, 1) : n;
}