import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DataTableModule } from 'angular2-datatable';
import { MyDatePickerModule } from 'mydatepicker';

// Forms Component
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

// Modal Component
import { ModalModule } from 'ngx-bootstrap/modal';

//Visits component
import { VisitPersonComponent } from './visit-person/visit-person.component';
import { VisitVehicleComponent } from './visit-vehicle/visit-vehicle.component';

// Tabs Component
import { TabsModule } from 'ngx-bootstrap/tabs';

// Components Routing
import { ComponentsRoutingModule } from './components-routing.module';
import { LettersComponent } from './letters/letters.component';
import { BuildingCommonSpacesComponent } from './building-common-spaces/building-common-spaces.component';
import { BuildingIncidentsComponent } from './building-incidents/building-incidents.component';
import { DeparmentResidentComponent } from './deparment-resident/deparment-resident.component';
import { DeparmentOwnerComponent } from './deparment-owner/deparment-owner.component';
import { DeparmentPaymentComponent } from './deparment-payment/deparment-payment.component';
import { DeparmentPenaltyComponent } from './deparment-penalty/deparment-penalty.component';

//Services
import { VehicleVisitService } from './../services/vehicle-visit.service';
import { LettersService } from './../services/letters.service';
import { DepartmentsService } from './../services/departments.service';
import { OwnerService } from './../services/owner.service';
import { IncidentService } from './../services/incident.service';
import { PersonVisitService } from './../services/person-visit.service';
import { SettingsComponent } from './settings/settings.component';

//Loading plugin
import { LoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    ComponentsRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DataTableModule,
    MyDatePickerModule,
    LoadingModule
  ],
  declarations: [
    VisitPersonComponent,
    VisitVehicleComponent,
    LettersComponent,
    BuildingCommonSpacesComponent,
    BuildingIncidentsComponent,
    DeparmentResidentComponent,
    DeparmentOwnerComponent,
    DeparmentPaymentComponent,
    DeparmentPenaltyComponent,
    SettingsComponent    
  ],
  providers: [
    OwnerService, 
    DepartmentsService, 
    LettersService, 
    VehicleVisitService,
    PersonVisitService,
    IncidentService    
  ]
})
export class ComponentsModule { }
