import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Owner } from './../../models/owner.model';
import { Department } from './../../models/department.model';

import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';

@Component({
  selector: 'app-deparment-owner',
  templateUrl: './deparment-owner.component.html',
  styles: ['../../shared/style.css']  
})
export class DeparmentOwnerComponent implements OnInit {  
  deparmentsFree: Department[];
  allDepartments: number[];
  processStatus = "search";
  departmentToSearch = 0;
  ownerSearch: Owner;
  registerOwner: FormGroup;

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {        
    this.db.list('departments').valueChanges().subscribe(
      data => this.allDepartments = <number[]>data
    );
    this.registerOwner = new FormGroup({
      name: new FormControl('', Validators.required),
      rut: new FormControl('', Validators.required),
      email: new FormControl('', Validators.email),
      address: new FormControl(''),
      cellphone: new FormControl('', Validators.required),
      department: new FormControl({ value: 0, disabled: true }, Validators.required)
    });
  }

  searchDepartmentData() {
    this.db.object('owners/' + this.departmentToSearch).valueChanges().subscribe(
      (data: any) => {
        if(data){
          this.processStatus = "found";
          this.ownerSearch = <Owner>data;
        } else {
          this.processStatus = "notFound";
        }
      }
    );    
  }  

  registerOwnerData(){
    this.processStatus = "registerData";
    this.registerOwner.controls['department'].setValue(this.departmentToSearch);
  }

  saveNewOwner(){    
    let formValue = this.registerOwner.getRawValue();
    let newOwner: Owner = new Owner(formValue.name, formValue.rut, formValue.email, formValue.address, 
                                   formValue.cellphone);    
    this.db.object('owners/' + formValue.department).set(newOwner).then(
      data => this.processStatus = "found");
  }

  onClear(){
    this.registerOwner.controls['name'].reset();
    this.registerOwner.controls['rut'].reset();
    this.registerOwner.controls['email'].reset();
    this.registerOwner.controls['address'].reset();
    this.registerOwner.controls['cellphone'].reset();
  }

  onEdit(){
    this.registerOwner.setValue({
      name: this.ownerSearch.name,
      rut: this.ownerSearch.rut,
      email: this.ownerSearch.email,
      address: this.ownerSearch.address,
      cellphone: this.ownerSearch.cellphone,
      department: this.departmentToSearch
    });
    this.processStatus = "registerData";    
  }

  onSearch(){
    this.processStatus = "search";
    this.departmentToSearch = 0;
  }

}
