import { Incident } from './../../models/incident.model';
import { IncidentService } from './../../services/incident.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-building-incidents',
  templateUrl: './building-incidents.component.html',
  styles: ['../../shared/style.css']
})
export class BuildingIncidentsComponent implements OnInit {
  @ViewChild('newIncidentModal') newIncidentModal:ModalDirective;  
  @ViewChild('confirmDeleteModal') deleteModal:ModalDirective;
  newIncidentForm: FormGroup;
  incidentData: Incident[] = [];
  selectedIncident: string;
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab'},
    monthLabels: { 1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic' },
    todayBtnTxt: 'Hoy'
  };
  typeList = [
    { name: 'Comunidad', value: 1 },
    { name: 'Interna', value: 2 }
  ];

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('incidents').snapshotChanges().map(actions => {
      return actions.map(action => ({ key: action.key, ...action.payload.val() }));
    }).subscribe(data => this.incidentData = <Incident[]>data );
    this.initializeForm();
  }

  initializeForm(){
    let partialDate = new Date();
    this.newIncidentForm = new FormGroup({
      incidentDate: new FormControl(null, Validators.required),
      description: new FormControl('', Validators.required),
      type: new FormControl(1, Validators.required),
    });         
  }

  showModal(){
    this.newIncidentModal.show();
  }

  saveIncident(){
    let formValue = this.newIncidentForm.value;    
    let newIncident: Incident = new Incident(formValue.description, formValue.incidentDate, formValue.type);
    this.db.list('/incidents').push(newIncident).then(
      data => {          
        this.initializeForm();
        this.newIncidentModal.hide();
      });    
  }

  showDeleteModal(key: string){
    this.selectedIncident = key;
    this.deleteModal.show();
  }

  onDelete(){    
    this.db.list('incidents').remove(this.selectedIncident).then(result => console.log("item deleted"));
    this.deleteModal.hide();
  }

}
