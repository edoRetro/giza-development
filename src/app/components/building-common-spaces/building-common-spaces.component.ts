import { Comment } from './../../models/comment.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-building-common-spaces',
  templateUrl: './building-common-spaces.component.html',
  styles: ['../../shared/style.css']
})
export class BuildingCommonSpacesComponent implements OnInit {

  constructor() { }

  laundryState: string = "Abierta";
  laundryAvailableNumber: number = 4;
  laundryTotalNumber: number = 5;
  lastLaundryComments: Comment[] = [
    new Comment(new Date(2017,6,14), "Se registra filtración en lavandería."),
    new Comment(new Date(2017,6,14), "Gásfiter llega a reparar filtración.")
  ];

  gymState: string = "Abierta";
  gymMachineAvailableNumber: number = 2;
  gymMachineTotalNumber: number = 3;
  lastGymComments: Comment[] = [
    new Comment(new Date(2017,6,14), "Se repara máquina trotadora. Se pone a disposición de la comunidad."),
    new Comment(new Date(2017,6,14), "Se agregan nuevas colchonetas al inventario."),
    new Comment(new Date(2017,6,14), "Se registra daño en colchonetas nuevas."),
    new Comment(new Date(2017,6,14), "Se agregan dos pesas de 5KG a inventario de gimnasio.")
  ];

  ngOnInit() {
  }

  createNewComment(){
    console.log("new coment");
  }

}
