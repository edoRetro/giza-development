import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { ModalDirective } from 'ngx-bootstrap';

import { ComboItem } from './../../models/combo-item';
import { Department } from './../../models/department.model';
import { DepartmentsService } from './../../services/departments.service';
import { Letter } from './../../models/letter.model';
import { LettersService } from './../../services/letters.service';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';

import * as DateTools from '../../utils/dateTools';


@Component({
  selector: 'app-letters',
  templateUrl: './letters.component.html',
  styles: ['../../shared/style.css']  
})
export class LettersComponent implements OnInit {
  @ViewChild('confirmDeleteModal') deleteModal:ModalDirective;
  @ViewChild('confirmDeliveredModal') deliveredModal:ModalDirective;
  @ViewChild('letterEditAndCreate') editAndCreateModal:ModalDirective;  

  allDeps: number[];
  allLetters: Letter[];
  selectedLetter: string;
  editMode: boolean = false;
  oldValues: Letter;

  editSaveLetterForm: FormGroup;

  typeList = [
    { name: 'Carta', value: 1 },
    { name: 'Paquete', value: 2 }    
  ];  
  sizeList = [
    { name: 'Pequeño', value: 1 },
    { name: 'Mediano', value: 2 },
    { name: 'Grande', value: 3 }
  ];
  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {su: 'Dom', mo: 'Lun', tu: 'Mar', we: 'Mie', th: 'Jue', fr: 'Vie', sa: 'Sab'},
    monthLabels: { 1: 'Ene', 2: 'Feb', 3: 'Mar', 4: 'Abr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dic' },
    todayBtnTxt: 'Hoy'
  };  
  
  constructor(private letterServ: LettersService, private depServ: DepartmentsService,
              private db: AngularFireDatabase) { }

  ngOnInit() {        
    this.initializeForm();
    this.db.list('letters').snapshotChanges().map(actions => {
      return actions.map(action => ({ key: action.key, ...action.payload.val() }));
    }).subscribe(data => this.allLetters = <Letter[]>data );

    this.db.list('departments').valueChanges().subscribe(
      data => this.allDeps = <number[]>data
    );    
  }

  initializeForm(){
    let partialDate = new Date();
    this.editSaveLetterForm = new FormGroup({
      department: new FormControl(0, Validators.required),
      receptionDate: new FormControl({date: DateTools.getDateObjectFromDate(new Date())}, Validators.required),
      type: new FormControl(1, Validators.required),
      size: new FormControl(1, Validators.required),
      description: new FormControl(''),
      deliveryCompany: new FormControl(''),
      isDelivered: new FormControl(false)
    });         
  }
  
  showDeleteModal(key: string){
    this.selectedLetter = key;        
    console.log(this.selectedLetter);
    this.deleteModal.show();
  }

  onDelete(){    
    this.db.list('letters').remove(this.selectedLetter).then(result => console.log("item deleted"));
    this.allLetters = this.letterServ.getLetters();
    this.deleteModal.hide();
  }

  showDeliveredModal(key: string){
    this.selectedLetter = key;
    this.deliveredModal.show();
  }

  onDelivered(){
    let result: Letter;
    this.db.object('letters/' + this.selectedLetter).valueChanges().subscribe(
      data => {
        result = <Letter>data;
        result.isDelivered = true;
        this.db.object('letters/' + this.selectedLetter).set(result).then(
          data => {
            console.log("Updated letter!");
            this.deliveredModal.hide();
          }).catch(
            error => { 
              console.log("Error updating!")
              this.deliveredModal.hide();
          });
      });    
  }

  createLetter(){
    this.editMode = false;
    this.editAndCreateModal.show();
  }

  editLetter(item: Letter){
    this.editMode = true;
    this.oldValues = item;    
    let aux = { date: DateTools.getDateObjectFromString(this.oldValues.receptionDate) };
    console.log(JSON.stringify(aux));
    this.editSaveLetterForm.setValue({
      department: this.oldValues.department,
      receptionDate: { date: DateTools.getDateObjectFromString(this.oldValues.receptionDate) },
      type: this.oldValues.type,
      size: this.oldValues.size,
      description: this.oldValues.description,
      deliveryCompany: this.oldValues.deliveryCompany,
      isDelivered: this.oldValues.isDelivered
    });
    this.editAndCreateModal.show();
  }  

  onCreateOrEdit(){
    let formValue = this.editSaveLetterForm.value;
    console.log(formValue.receptionDate.formatted);
    let newLetter: Letter = new Letter(formValue.department, formValue.receptionDate.formatted, formValue.type, formValue.size, formValue.description, 
                                        formValue.deliveryCompany, formValue.isDelivered);
    
    if(!this.editMode) { //If im creating a letter
      this.db.list('/letters').push(newLetter).then(
        data => {          
          this.initializeForm();
          this.editAndCreateModal.hide();
        });
    } else { //Im editing a letter      
      this.db.object('letters/' + this.oldValues.key).set(newLetter).then(
        data => {          
          this.initializeForm();
          this.editAndCreateModal.hide();
        });
    }    
  }

}
