import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisitPersonComponent } from './visit-person/visit-person.component';
import { VisitVehicleComponent } from './visit-vehicle/visit-vehicle.component';
import { BuildingIncidentsComponent } from './building-incidents/building-incidents.component';
import { BuildingCommonSpacesComponent } from './building-common-spaces/building-common-spaces.component';
import { LettersComponent } from './letters/letters.component';
import { DeparmentPenaltyComponent } from './deparment-penalty/deparment-penalty.component';
import { DeparmentPaymentComponent } from './deparment-payment/deparment-payment.component';
import { DeparmentOwnerComponent } from './deparment-owner/deparment-owner.component';
import { DeparmentResidentComponent } from './deparment-resident/deparment-resident.component';
import { SettingsComponent } from 'app/components/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Módulos'
    },
    children: [
      {
        path: 'department-resident',
        component: DeparmentResidentComponent,
        data: {
          title: 'Residentes'
        }
      },
      {
        path: 'department-owner',
        component: DeparmentOwnerComponent,
        data: {
          title: 'Dueños'
        }
      },
      {
        path: 'department-payment',
        component: DeparmentPaymentComponent,
        data: {
          title: 'Gastos comunes'
        }
      },
      {
        path: 'department-penalty',
        component: DeparmentPenaltyComponent,
        data: {
          title: 'Multas'
        }
      },
      {
        path: 'visit-person',
        component: VisitPersonComponent,
        data: {
          title: 'Visita personas'
        }
      },
      {
        path: 'visit-vehicle',
        component: VisitVehicleComponent,
        data: {
          title: 'Visita vehículo'
        }
      },
      {
        path: 'letters',
        component: LettersComponent,
        data: {
          title: 'Encomiendas y paquetes'
        }
      },
      {
        path: 'building-commonSpaces',
        component: BuildingCommonSpacesComponent,
        data: {
          title: 'Espacios comunes'
        }
      },
      {
        path: 'building-incident',
        component: BuildingIncidentsComponent,
        data: {
          title: 'Incidencias'
        }
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: {
          title: 'Configuración'
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],  
})
export class ComponentsRoutingModule {}
