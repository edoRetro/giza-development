import { Visit } from './../../models/visit.model';
import { PersonVisitService } from './../../services/person-visit.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-visit-person',
  templateUrl: './visit-person.component.html',
  styles: ['../../shared/style.css']
})
export class VisitPersonComponent implements OnInit {
  visitData: Visit[] = [];
  loading = false;

  constructor(private sVisits: PersonVisitService, private db: AngularFireDatabase) { }  

  ngOnInit() {
    this.loading = true;    
    this.db.list('personVisits').valueChanges().subscribe(
      data => { 
        this.visitData = <Visit[]>data;
        this.loading = false;
      }
    );
  }

  showUpdateModal(item){
    console.log("Show update modal");
  }

  showDeleteModal(item){
    console.log("Show delete modal");
  }

  showCreateModal(){
    console.log("Show create modal");
  }

}
