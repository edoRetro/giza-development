import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Resident } from 'app/models/resident.model';

@Component({
  selector: 'app-deparment-resident',
  templateUrl: './deparment-resident.component.html',
  styles: ['../../shared/style.css']  
})
export class DeparmentResidentComponent implements OnInit {

  processStatus = "search";
  allDepartments: number[];
  departmentToSearch = 0;
  residentData: Resident;

  constructor(private db: AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('departments').valueChanges().subscribe(
      data => this.allDepartments = <number[]>data
    );
  }

  searchDepartmentData() {
    this.db.object('residents/' + this.departmentToSearch).valueChanges().subscribe(
      (data: any) => {
        if(data){
          this.residentData = <Resident>data;
          this.processStatus = "found";
        } else this.processStatus = "notFound";
      }
    );    
  } 

}
