import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styles: ['../../shared/style.css']
})
export class SettingsComponent implements OnInit {

  constructor() { }

  incidentData = [];

  ngOnInit() {
    this.incidentData = [];
  }

}
