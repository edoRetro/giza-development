import { Visit } from './../../models/visit.model';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-visit-vehicle',
  templateUrl: './visit-vehicle.component.html',
  styles: ['../../shared/style.css']
})
export class VisitVehicleComponent implements OnInit {
  visitsData: Visit[];
  historyData: Visit[];
  loading = false;

  constructor(private db: AngularFireDatabase) { }  

  ngOnInit() {
    this.loading = true;    
    this.db.list('vehicleVisits').valueChanges().subscribe(
      data => { 
        this.visitsData = <Visit[]>data;
        this.loading = false;
      }
    );
  }

  onCreateVisit(index: number){
    console.log("Create visit");
  }  

  onUpdateVisit(item){
    console.log("Update visit");
  }

  showDeleteModal(index: number){
    console.log("Show delete modal");
  }

  showFreeModal(index: number){
    console.log("Show free modal");
  }

}
