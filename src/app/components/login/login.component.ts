import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',  
  styles: ['../../shared/style.css']
})
export class LoginComponent implements OnInit {
  loading = false;
  loginForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private authSrv: AuthService ) {}  

  ngOnInit(){
    this.initializeForm();
  }

  initializeForm(){
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)      
    });  
  }

  onLogin(){
    this.loading = true;
    let formValue = this.loginForm.value;    
    this.authSrv.emailLogin(formValue.username, formValue.password).then(
      data => { 
        this.loading = false;
        if (data){
          this.router.navigate(['/dashboard'], {relativeTo: this.route});
        } else {
          console.log("Error at logins");
        }         
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }  
}
