import { Department } from './department.model';

export class Letter {        

    public key: string;

    constructor(public department: Department, 
                public receptionDate: string, 
                public type: number,  
                public size: number, 
                public description: string, 
                public deliveryCompany: string, 
                public isDelivered: boolean){
    }
}
