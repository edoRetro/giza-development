export class Resident {

    public key: string;

    constructor(
        public department: number,
        public adress: string,
        public cellphone: string,
        public email: string,
        public name: string,
        public rut: string){}
}