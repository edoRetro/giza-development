import { Department } from './department.model';

export class Visit {
    //common properties
    public accessDate: Date;
    public accessTime: string;    
    public department: number;
    public personName: string;    
    public personRut: string;
    //vehicle only
    public usedSpaceNumber: number;
    public carRegNumber: string;
    public parkingStatus: number; //0: inactivo, 1: en tiempo, 2: fuera de tiempo    

    constructor(){}
}
