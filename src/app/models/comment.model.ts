export class Comment {
    public registerDate: Date;
    public comment: string;

    constructor(dateParam: Date, commentParam: string) {
        this.registerDate = dateParam;
        this.comment = commentParam;
    }    
}
