export class Incident {

    public key: string;

    constructor(public descPar: string, 
                public regDatePar: string,
                public type: number){
    }
}
