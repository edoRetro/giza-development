export class ComboItem {
    public id: number;
    public desc: string;

    constructor(idParam: number, descParam: string){
        this.id = idParam;
        this.desc = descParam;
    }
}
