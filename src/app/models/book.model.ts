export class Book {
    public title: string;
    public description: string;
    public price: number;
    public author: string;

    constructor(){}
}