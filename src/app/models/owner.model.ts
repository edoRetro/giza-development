export class Owner {            
    constructor(public name: string, 
                public rut: string, 
                public email: string, 
                public address: string, 
                public cellphone: string) {         
    }    
}
