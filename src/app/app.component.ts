import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {   
}

