import { Department } from './../models/department.model';
import { Visit } from './../models/visit.model';
import { Injectable } from '@angular/core';

@Injectable()
export class VehicleVisitService {

  constructor() { }

  private activeVisitList: Visit[] = [
    // new Visit(1, new Department(814), "Eduardo Plaza", "VBSA24", new Date(2017, 6, 7, 13, 20), 1, "17413554-2"),
    // new Visit(2, new Department(813), "Verónica González", "ASD32", new Date(2017, 6, 7, 12, 15), 1, "13229564-5"),
    // new Visit(3, null, "", "", null, 0, ""),
    // new Visit(4, new Department(813), "Bombo Fica", "WETR84", new Date(2017, 6, 7, 9, 10), 2, "13246591-4"),
  ];

  private historyList: Visit[] = [
    // new Visit(2, new Department(106), "Alberto Plaza", "WWMR34", new Date(2017, 6, 1, 18, 20), 0, "10745441-2"),
    // new Visit(2, new Department(312), "Lucho Jara", "POLL11", new Date(2017, 6, 2, 7, 1), 0, "10206559-k"),
    // new Visit(3, new Department(810), "Pancho Melo", "AW2131", new Date(2017, 6, 2, 17, 20), 0, "11449594-5"),
    // new Visit(1, new Department(1504), "Chupa Melo", "RT4564", new Date(2017, 6, 3, 17, 25), 0, "16594653-1"),
  ];

  getVisits(): Visit[] {
    return this.activeVisitList.slice();
  }

  getHistory(): Visit[] {
    return this.historyList.slice();
  }

  addVisit(newVisit: Visit, index: number): Visit[] {    
    this.activeVisitList[index] = newVisit;
    return this.activeVisitList.slice();
  }

  freeVisit(index: number){
    //let spaceNumber: number = this.activeVisitList[index].space;
    //this.activeVisitList[index] = new Visit(spaceNumber, null, "", "", null, 0, "");
  }

}
