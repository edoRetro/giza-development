import { Injectable } from '@angular/core';
import { Department } from './../models/department.model';
import { Visit } from './../models/visit.model';

@Injectable()
export class PersonVisitService {

  constructor() { }

  private visitList: Visit[] = [
    // new Visit(null, new Department(214), "Eduardo Plaza", "", new Date(2017, 6, 7, 13, 20), 0, "17413554-2"),
    // new Visit(null, new Department(1003), "Verónica González", "", new Date(2017, 6, 7, 12, 15), 0, "13229564-5"),    
    // new Visit(null, new Department(208), "Mauricio Del Rio", "", new Date(2017, 6, 7, 9, 10), 0, "13246591-4"),
    // new Visit(null, new Department(701), "Pepito Jose Maureira", "", new Date(2017, 6, 7, 9, 10), 0, "13246591-4"),
    // new Visit(null, new Department(506), "Laura Kent", "", new Date(2017, 6, 10, 8, 25), 0, "18455200-9"),
    // new Visit(null, new Department(903), "Camila Neira", "", new Date(2017, 6, 10, 8, 40), 0, "13571411-0"),
  ];

  getVisits(): Visit[] {
    return this.visitList.slice();
  }

}
