import { Department } from './../models/department.model';
import { Letter } from './../models/letter.model';
import { Injectable } from '@angular/core';

@Injectable()
export class LettersService {

  constructor() { }

  private letters: Letter[] = [
    // new Letter(new Department(814), new Date(2017, 5, 24), 2, 2, "Caja de dafiti", "Correos de chile", true),
    // new Letter(new Department(814), new Date(2017, 5, 22), 2, 1, "Paquete pequeño", "Tur-bus", false),
    // new Letter(new Department(810), new Date(2017, 5, 19), 1, 1, "Cuenta luz", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
    // new Letter(new Department(811), new Date(2017, 5, 10), 1, 1, "Cuenta autopista", "Correos", false),
  ];

  getLetters() : Letter[] {
    return this.letters.slice();
  }

  deleteLetter(index: number){
    this.letters.splice(index, 1);
  }

  markAsDelivered(index: number){
    this.letters[index].isDelivered = true;
  }

}
