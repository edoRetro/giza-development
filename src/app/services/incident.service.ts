import { Incident } from './../models/incident.model';
import { Injectable } from '@angular/core';

@Injectable()
export class IncidentService {

  constructor() { }

  private dataList: Incident[] = [
    // new Incident("Se deja constancia sobre filtración en lavandería.", new Date(2017,9,6,9,32)),
    // new Incident("Jardinero llega al edificio.", new Date(2017,9,6,9,48)),
    // new Incident("Jardinero se retira.", new Date(2017,9,6,10,50)),
    // new Incident("Ascensor 1 con problemas. Se llama al técnico y se deja fuera de servicio por el momento.", new Date(2017,9,6,10,58)),
    // new Incident("Cambio de turno.", new Date(2017,9,6,11,15)),
    // new Incident("Se deja constancia sobre filtración en estacionamiento.", new Date(2017,9,6,17,15)),
  ];

  getAllIncidents(): Incident[] {
    return this.dataList.slice();
  }

}
