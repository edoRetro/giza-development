import { Injectable } from '@angular/core';

import { OwnerService } from './owner.service';
import { Department } from './../models/department.model';

@Injectable()
export class DepartmentsService {

  constructor(private ownerService: OwnerService) {}

  private departmentsList: Department[] = [
    new Department(810),
    new Department(811),
    new Department(812),
    new Department(813),
    new Department(814),
  ];

  getAllDepartments(): Department[] {
    return this.departmentsList.slice();
  }

  getAllDepartmentsFromOwners(): Promise<Department[]> {
    return new Promise((resolve, reject) => {
      let depList: Department[] = [];      
      let ownerList = this.ownerService.getOwners();      
      ownerList.forEach(ownerSingle => {
        // depList.push(ownerSingle.department);
      });
      resolve(depList);
    });
  }

  getDepartmentsWithoutOwner(): Promise<Department[]>{
    return new Promise((resolve, reject) => {
      let allDepList = this.getAllDepartments();
      this.getAllDepartmentsFromOwners().then(
        (result: Department[]) => {
          result.forEach(resSingle => {            
            allDepList.forEach((depSingle, i) => {
              if(depSingle.departmentId == resSingle.departmentId) {
                allDepList.splice(i, 1);                
              }
            });
          });          
          resolve(allDepList);
        })
    });
  }

}
