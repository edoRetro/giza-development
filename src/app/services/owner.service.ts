import { Injectable } from '@angular/core';

import { Department } from './../models/department.model';
import { Owner } from './../models/owner.model';

@Injectable()
export class OwnerService {

  private owners: Owner[] = [
    // new Owner("Eduardo Plaza", "17413591-6", "edo.plaza@outlook.cl", "Santa Rosa 325", "88674859", new Department(814)),
    // new Owner("Verónica González", "18366771-8", "gonzalezgallegosv@gmail.com", "Santa Rosa 325", "87714578", new Department(813))
  ];

  getOwners(): Owner[]{
    return this.owners.slice();
  }

  setOwner(ownerInput, index) {
    this.owners[index] = ownerInput;
  }

  getOwnerByDepartmentId(depId) : Promise<Owner> {        
    return new Promise((resolve, reject) => {      
        this.owners.forEach(ownerSingle => {
          // if(ownerSingle.department.departmentId == depId){              
          //     resolve(ownerSingle);
          //   }
        });  
        reject("No results");    
    });    
  }

  createOrUpdateOwner(ownerInput : Owner) : Promise<boolean> {
    return new Promise((resolve, reject) => {      
      this.owners.forEach((ownerSingle, index) => {
        if(ownerSingle.rut == ownerInput.rut) {
          this.setOwner(ownerInput, index);          
          resolve(true);
        }
      })
      // this.owners.push(ownerInput);
      // console.log("Created:" + JSON.stringify(ownerInput));
      // resolve(true);
    });
  }

}
